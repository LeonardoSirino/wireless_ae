#include "heltec.h"
#include "HardwareSerial.h"
#include "BluetoothSerial.h"

#define BAND 433E6 //you can set band here directly,e.g. 868E6,915E6
#define RED 21
#define GREEN 13
#define BLUE 12
#define THR 2000

String rssi = "RSSI --";
String packSize = "--";
String packet;

BluetoothSerial SerialBT;
String BTdata;

void setup()
{
  Serial.begin(115200);
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);

  Heltec.display->init();
  Heltec.display->flipScreenVertically();
  Heltec.display->setFont(ArialMT_Plain_24);
  Heltec.display->drawString(0, 0, "Inicio do");
  Heltec.display->drawString(0, 24, "Receiver!");
  Heltec.display->display();
  delay(500);

  SerialBT.begin("AE ASL");
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_16);
  Heltec.display->drawString(0, 0, "Inicio do BT");
  Heltec.display->display();

  delay(500);

  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);

  blink_led(5, 50, RED);
  blink_led(5, 50, GREEN);
  blink_led(5, 50, BLUE);
  test_led();
}

void loop()
{
  int packetSize = LoRa.parsePacket();
  if (packetSize)
  {
    blink_led(1, 100, BLUE);
    Serial.println("Dados recebidos");
    cbk(packetSize);
  }

  char c = SerialBT.read();
  if (String(c) == "*")
  {
    test_led();
  }

  delay(10);
}

void LoRaData()
{
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_24);
  Heltec.display->drawString(0, 0, rssi);
  Heltec.display->drawStringMaxWidth(0, 24, 128, packet);
  Heltec.display->display();
}

void cbk(int packetSize)
{
  packet = "";
  packSize = String(packetSize, DEC);
  for (int i = 0; i < packetSize; i++)
  {
    packet += (char)LoRa.read();
  }
  int index = packet.indexOf(";");
  int ASL = packet.substring(0, index).toInt();
  SerialBT.print(String(ASL) + "\r\n");
  if (ASL > THR)
  {
    digitalWrite(RED, HIGH);
  }
  else
  {
    digitalWrite(RED, LOW);
  }
  rssi = "RSSI " + String(LoRa.packetRssi(), DEC);
  LoRaData();
}

void blink_led(int num, int time, int LED)
{
  for (int i = 0; i < num; i++)
  {
    digitalWrite(LED, HIGH);
    delay(time);
    digitalWrite(LED, LOW);
    delay(time);
  }
}

void test_led()
{
  for (int i = 0; i < 5; i++)
  {
    Heltec.display->clear();
    Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
    Heltec.display->setFont(ArialMT_Plain_16);
    Heltec.display->drawString(0, 0, "Teste do");
    Heltec.display->drawString(0, 16, "LED");
    Heltec.display->display();
    digitalWrite(RED, HIGH);
    digitalWrite(GREEN, HIGH);
    digitalWrite(BLUE, HIGH);
    delay(50);
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    digitalWrite(BLUE, LOW);
    delay(50);
  }
}
