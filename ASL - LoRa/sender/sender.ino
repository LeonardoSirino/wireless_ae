//-----------------//
// INCLUDES
//-----------------//
#include "time.h"
#include "math.h"
#include "heltec.h"
#include <Wire.h>
#include <Adafruit_ADS1015.h>

//-----------------//
// DEFINITIONS
//-----------------//

#define BAND 433E6 //you can set band here directly,e.g. 868E6,915E6
#define RED 25
#define GREEN 13
#define BLUE 12

// parâmetros da aquisição
#define SAMPLE_RATE 2
#define THR 2000

//-----------------//
// GLOBAL VARIABLES
//-----------------//
Adafruit_ADS1115 ads;
int acq_micro_delay = int(1E6 / SAMPLE_RATE);
volatile bool acquire = false;
String rssi = "RSSI --";
String packSize = "--";
String received_packet;

long int ASL = 0;
float T_send = 0;

// Identificação do ESP
char ssid1[15];
char ssid2[15];
String ssidESP = "";

//Infos de status
String mode = "wait";
bool acq_done = false;
bool has_data = false;
bool reset_data = true;

// variaveis para debug
int count_reads = 0;

void setup()
{
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  delay(1000);

  ads.begin(17, 23);
  ads.setGain(GAIN_SIXTEEN);

  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);

  Serial.begin(115200);
  Serial.println("\nEA ASL iniciado");

  //Obtem dados do chip
  uint64_t chipid = ESP.getEfuseMac(); // The chip ID is essentially its MAC address(length: 6 bytes).
  uint16_t chip = (uint16_t)(chipid >> 32);

  snprintf(ssid1, 15, "%04X", chip);
  snprintf(ssid2, 15, "%08X", (uint32_t)chipid);
  ssidESP = String(ssid1) + String(ssid2);

  // Inicio da tarefa que fará a leitura dos pinos analógicos
  xTaskCreatePinnedToCore(
      ReadAnalog,   /* Function to implement the task */
      "ReadAnalog", /* Name of the task */
      10000,        /* Stack size in words */
      NULL,         /* Task input parameter */
      1,            /* Priority of the task */
      NULL,         /* Task handle. */
      1);           /* Core where the task should run */
}

void loop()
{
  mode = "Acquire";
  while (true)
  {

    if (ASL > THR)
    {
      digitalWrite(RED, HIGH);
    }
    else
    {
      digitalWrite(RED, LOW);
    }
    send_data(ASL, T_send);
  }
}

void send_data(int asl, float t_send)
{
  Serial.println("ASL: " + String(ASL));

  int t0 = esp_timer_get_time();

  LoRa.beginPacket();
  LoRa.print(String(asl) + ";" + String(t_send));
  LoRa.endPacket();

  int t1 = esp_timer_get_time();
  Serial.println("Time to send: " + String(t1 - t0));
  Serial.println("");

  blink_led(1, 100, BLUE);
}

void ReadAnalog(void *pvParameters)
{
  // buffers de leitura dos Pinos e contador
  int buffer = 0;
  int raw_count = 0;

  // tempos
  unsigned long t0_acq = 0;
  unsigned long t_close = 0;
  unsigned long t_acq = 0;
  unsigned long t_init = 0;

  // valores medios das leituras nos pinos
  int mean = 0;
  unsigned int queue_count = 0;
  unsigned int post_count = 0;

  // variaveis para debug
  int total_count = 0;

  t0_acq = 0;
  while (true)
  {
    if (mode == "Acquire")
    {
      if (reset_data)
      { // aquisição iniciada
        Serial.println("Resetando dados da aquisição");
        t0_acq = esp_timer_get_time();
        t_close = acq_micro_delay;
        raw_count = 0;
        total_count = 0;
        reset_data = false;
        acq_done = false;
        queue_count = 0;
        post_count = 0;
        buffer = 0;
        mean = 0;
        t_init = 0;
      }

      t_acq = esp_timer_get_time();

      if (t_acq - t0_acq <= t_close)
      { // ainda não fechou o tempo da média
        int16_t ch_0 = ads.readADC_SingleEnded(0);
        buffer += abs(ch_0);
        raw_count++;
        total_count++;
      }
      else
      { // temp da media encerrado
        mean = buffer / raw_count;

        ASL = mean;
        T_send = t_init / 1E6;

        buffer = abs(ads.readADC_SingleEnded(0));
        t_init = esp_timer_get_time();

        raw_count = 1;
        t_close += acq_micro_delay;
      }
    }
    else if (mode == "wait")
    {
      vTaskDelay(200);
    }
    else
    {
      Serial.println("Modo não previsto!");
      vTaskDelay(2000);
    }
  }
}

void blink_led(int num, int time, int LED)
{
  for (int i = 0; i < num; i++)
  {
    digitalWrite(LED, HIGH);
    delay(time);
    digitalWrite(LED, LOW);
    delay(time);
  }
}

bool wait_for_response(int tries)
{
  bool try_again = true;
  int k = 0;
  String packet = "";
  while (try_again)
  {
    int packetSize = LoRa.parsePacket();
    if (packetSize)
    {
      packSize = String(packetSize, DEC);
      for (int i = 0; i < packetSize; i++)
      {
        packet += (char)LoRa.read();
      }
    }
    if (packet != "" || k > tries)
    {
      try_again = false;
    }
    else
    {
      delay(300);
      k++;
    }
  }

  bool received = packet != "";
  if (received)
  {
    Serial.println(packet);
    received_packet = packet;
  }
  return received;
}