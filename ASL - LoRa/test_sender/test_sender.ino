//-----------------//
// INCLUDES
//-----------------//
#include "time.h"
#include "math.h"
#include <Wire.h>
#include <Adafruit_ADS1015.h>
#include "BluetoothSerial.h"

//-----------------//
// DEFINITIONS
//-----------------//

#define RED 25
#define GREEN 13
#define BLUE 12

// parâmetros da aquisição
#define SAMPLE_RATE 5
#define THR 2000

//-----------------//
// GLOBAL VARIABLES
//-----------------//
Adafruit_ADS1115 ads;
int acq_micro_delay = int(1E6 / SAMPLE_RATE);
volatile bool acquire = false;

long int ASL = 0;
long int max_value = 0;
long int max_to_send = 0;
float T_send = 0;

BluetoothSerial SerialBT;
String BTdata;

// Identificação do ESP
char ssid1[15];
char ssid2[15];
String ssidESP = "";

//Infos de status
String mode = "wait";
bool acq_done = false;
bool has_data = false;
bool reset_data = true;

// variaveis para debug
int count_reads = 0;

void setup()
{
  Serial.println("Inicio do sistema\n\n\n");
  SerialBT.begin("AE");
  
  ads.begin();
  ads.setGain(GAIN_ONE);

  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);

  Serial.begin(115200);
  Serial.println("\nEA ASL iniciado");

  //Obtem dados do chip
  uint64_t chipid = ESP.getEfuseMac(); // The chip ID is essentially its MAC address(length: 6 bytes).
  uint16_t chip = (uint16_t)(chipid >> 32);

  snprintf(ssid1, 15, "%04X", chip);
  snprintf(ssid2, 15, "%08X", (uint32_t)chipid);
  ssidESP = String(ssid1) + String(ssid2);

  // Inicio da tarefa que fará a leitura dos pinos analógicos
  xTaskCreatePinnedToCore(
      ReadAnalog,   /* Function to implement the task */
      "ReadAnalog", /* Name of the task */
      10000,        /* Stack size in words */
      NULL,         /* Task input parameter */
      1,            /* Priority of the task */
      NULL,         /* Task handle. */
      1);           /* Core where the task should run */
}

void loop()
{
  mode = "Acquire";
  while (true)
  {
    if (has_data)
    {
      send_data(ASL);
      has_data = false;
    }
    else
    {
      delay(10);
    }
  }
}

void send_data(int asl)
{
  Serial.println("Raw: " + String(ASL));

  int t0 = esp_timer_get_time();
  float voltage = -0.000125 * asl + 4.096;
  float c_ASL = 20 * log10(voltage * 1E6) - 100;

  Serial.println("ASL: " + String(c_ASL));

  SerialBT.print(String(c_ASL));

  int t1 = esp_timer_get_time();
  Serial.println("Time to send: " + String(t1 - t0));
  Serial.println("");

  blink_led(1, 100, BLUE);
}

void ReadAnalog(void *pvParameters)
{
  // buffers de leitura dos Pinos e contador
  int buffer = 0;
  int raw_count = 0;

  // tempos
  unsigned long t0_acq = 0;
  unsigned long t_close = 0;
  unsigned long t_acq = 0;
  unsigned long t_init = 0;

  // valores medios das leituras nos pinos
  int mean = 0;
  unsigned int queue_count = 0;
  unsigned int post_count = 0;

  // variaveis para debug
  int total_count = 0;

  t0_acq = 0;
  while (true)
  {
    if (mode == "Acquire")
    {
      if (reset_data)
      { // aquisição iniciada
        Serial.println("Resetando dados da aquisição");
        t0_acq = esp_timer_get_time();
        t_close = acq_micro_delay;
        raw_count = 0;
        total_count = 0;
        reset_data = false;
        acq_done = false;
        queue_count = 0;
        post_count = 0;
        buffer = 0;
        mean = 0;
        t_init = 0;
      }

      t_acq = esp_timer_get_time();

      if (t_acq - t0_acq <= t_close)
      { // ainda não fechou o tempo da média
        int16_t ch_0 = ads.readADC_SingleEnded(0);
        buffer += abs(ch_0);
        raw_count++;
        total_count++;
        if (ch_0 > max_value)
        {
          max_value = ch_0;
        }
      }
      else
      { // temp da media encerrado
        mean = buffer / raw_count;
        max_to_send = max_value;
        max_value = 0;

        ASL = mean;
        T_send = t_init / 1E6;

        buffer = abs(ads.readADC_SingleEnded(0));
        t_init = esp_timer_get_time();

        raw_count = 1;
        t_close += acq_micro_delay;

        has_data = true;
      }
    }
    else if (mode == "wait")
    {
      vTaskDelay(200);
    }
    else
    {
      Serial.println("Modo não previsto!");
      vTaskDelay(2000);
    }
  }
}

void blink_led(int num, int time, int LED)
{
  for (int i = 0; i < num; i++)
  {
    digitalWrite(LED, HIGH);
    delay(time);
    digitalWrite(LED, LOW);
    delay(time);
  }
}