//-----------------//
// INCLUDES
//-----------------//
#include "time.h"
#include "math.h"
#include <WiFi.h>

//-----------------//
// DEFINITIONS
//-----------------//

// parâmetros da aquisição
#define SAMPLE_RATE 100 // 3.0 kHz de taxa de aquisição

// Pinos de leitura analógica
#define ANALOG_PIN 36

#define LED 2
#define SEND_SAMPLE_SIZE 10

// Estrutura dos dados mandados
struct asl_data
{
  int asl;
  unsigned long t0;
  unsigned long t1;
  int size;
};
String str_data;

//-----------------//
// GLOBAL VARIABLES
//-----------------//
int acq_micro_delay = int(1E6 / SAMPLE_RATE);
volatile bool acquire = false;

// Credenciais para acesso à rede do Lactec
const char *networkName = "Lactec-Guest";
const char *networkPswd = "lactec123";

// Endereço e porta para comunicação TCP
const char *host = "192.168.0.191";
const int port = 3333;

// Está conectado?
boolean connected = false;

// Client WiFi
WiFiClient client;

// Identificação do ESP
char ssid1[15];
char ssid2[15];
String ssidESP = "";

// filas para transmissão dos valores médios dos pinos
QueueHandle_t data_queue;

//Infos de status
String mode = "wait";
bool acq_done = false;
bool has_data = false;
bool reset_data = true;

// variaveis para debug
int count_reads = 0;

void setup()
{
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  Serial.begin(115200);
  Serial.println("\nEA ASL iniciado");

  // Conexão ao WiFi
  connectToWiFi(networkName, networkPswd);

  //Obtem dados do chip
  uint64_t chipid = ESP.getEfuseMac(); // The chip ID is essentially its MAC address(length: 6 bytes).
  uint16_t chip = (uint16_t)(chipid >> 32);

  snprintf(ssid1, 15, "%04X", chip);
  snprintf(ssid2, 15, "%08X", (uint32_t)chipid);
  ssidESP = String(ssid1) + String(ssid2);

  // Criação das filas
  data_queue = xQueueCreate(SEND_SAMPLE_SIZE * 10, sizeof(asl_data));
  Serial.println("Fila criada");

  if (data_queue == NULL)
  {
    Serial.println("Erro na criação da fila");
  }

  // Inicio da tarefa que fará a leitura dos pinos analógicos
  xTaskCreatePinnedToCore(
      ReadAnalog,   /* Function to implement the task */
      "ReadAnalog", /* Name of the task */
      10000,        /* Stack size in words */
      NULL,         /* Task input parameter */
      1,            /* Priority of the task */
      NULL,         /* Task handle. */
      1);           /* Core where the task should run */
}

void loop()
{
  int count = 0;
  BaseType_t status;
  asl_data data;

  while (not connected)
  {
    client.connect(host, port);
    delay(200);
  };
  Serial.println("Client conectado");
  mode = "Acquire";

  while (true)
  {
    status = xQueueReceive(data_queue, &data, 1);
    if (int(status) == 1)
    {
      if (data.asl > 100)
      {
        digitalWrite(LED, HIGH);
      }
      else
      {
        digitalWrite(LED, LOW);
      }
      str_data += String(data.asl) + ";" + String(data.size) + ";" + String(data.t0) + ";" + String(data.t1) + "&";
      Serial.println(str_data);
      count++;
      if (count == SEND_SAMPLE_SIZE)
      {
        if (not client.connected())
        {
          client.connect(host, port);
        }
        client.print(str_data + "\r\n");
        // Serial.println("Dados enviados: " + str_data);
        str_data = "";
        count = 0;
      }
    }
    else
    {
      delay(1);
    }
  }
}

//-----------------//
// FUNCTIONS
//-----------------//
void ReadAnalog(void *pvParameters)
{
  // buffers de leitura dos Pinos e contador
  int buffer = 0;
  int raw_count = 0;

  // tempos
  unsigned long t0_acq = 0;
  unsigned long t_close = 0;
  unsigned long t_acq = 0;
  unsigned long t_init = 0;

  // valores medios das leituras nos pinos
  int mean = 0;

  // Dados enviados
  asl_data send_data;

  unsigned int queue_count = 0;
  unsigned int post_count = 0;

  // variaveis para debug
  int total_count = 0;

  t0_acq = 0;
  while (true)
  {
    if (mode == "Acquire")
    {
      if (reset_data)
      { // aquisição iniciada
        Serial.println("Resetando dados da aquisição");
        t0_acq = esp_timer_get_time();
        t_close = acq_micro_delay;
        raw_count = 0;
        total_count = 0;
        reset_data = false;
        acq_done = false;
        queue_count = 0;
        post_count = 0;
        buffer = 0;
        mean = 0;
        t_init = 0;
      }

      t_acq = esp_timer_get_time();

      if (t_acq - t0_acq <= t_close)
      { // ainda não fechou o tempo da média
        buffer += analogRead(ANALOG_PIN);
        raw_count++;
        total_count++;
      }
      else
      { // temp da media encerrado
        mean = buffer / raw_count;

        send_data.asl = mean;
        send_data.size = raw_count;
        send_data.t0 = t_init;
        send_data.t1 = t_acq;

        buffer = analogRead(ANALOG_PIN);
        t_init = esp_timer_get_time();

        raw_count = 1;

        // escrita nas filas
        xQueueSendToBack(data_queue, (void *)&send_data, 0);
        queue_count++;

        t_close += acq_micro_delay;
      }
    }
    else if (mode == "wait")
    {
      vTaskDelay(200);
    }
    else
    {
      Serial.println("Modo não previsto!");
      vTaskDelay(2000);
    }
  }
}

void connectToWiFi(const char *ssid, const char *pwd)
{
  Serial.println("Connecting to WiFi network: " + String(ssid));

  // delete old config
  WiFi.disconnect(true);
  //register event handler
  WiFi.onEvent(WiFiEvent);

  //Initiate connection
  WiFi.begin(ssid, pwd);

  Serial.println("Waiting for WIFI connection...");
}

//wifi event handler
void WiFiEvent(WiFiEvent_t event)
{
  switch (event)
  {
  case SYSTEM_EVENT_STA_GOT_IP:
    //When connected set
    Serial.print("WiFi connected! IP address: ");
    Serial.println(WiFi.localIP());
    //initializes the UDP state
    //This initializes the transfer buffer
    client.connect(host, port);
    connected = true;
    break;
  case SYSTEM_EVENT_STA_DISCONNECTED:
    Serial.println("WiFi lost connection");
    connected = false;
    break;
  }
}
/*
String DataToSend()
{
  String data = "";
  int x;
  int y;
  int z;
  for (int i = 0; i < SEND_SAMPLE_SIZE; i++)
  {
    if (uxQueueMessagesWaiting(x_queue) == 0)
    {
      return data;
    }
    else
    {
      xQueueReceive(x_queue, &x, 0);
      xQueueReceive(y_queue, &y, 0);
      xQueueReceive(z_queue, &z, 0);
      data += String(x - x_offset) + ";" + String(y - y_offset) + ";" + String(z - z_offset) + "\n";
    }
  }
  return data;
}

void SendAll()
{
  bool cond = true;
  while (cond)
  {
    String data = DataToSend();
    if (data == "")
    {
      cond = false;
      SerialBT.print("#\r\n");
      Serial.println("Transmissão encerrada");
    }
    else
    {
      SerialBT.print(data + "\r\n");
    }
  }
}

void BlinkLed(int count, int period)
{
  for (int i = 0; i < count; i++)
  {
    digitalWrite(LED, HIGH);
    delay(period);
    digitalWrite(LED, LOW);
    delay(period);
  }
}
*/