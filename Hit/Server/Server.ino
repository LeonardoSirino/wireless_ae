#include "time.h"
#include <WiFi.h>

int LED = LED_BUILTIN;

//Parâmetros de EA
const int preTrigger = 200;
const int afterTrigger = 2000;
int hitLenght = preTrigger + afterTrigger;
float thr = 300;
int modo = 2;
int hitPointer = 0;
/*
 * Modo 0: aguardando Hits
 * Modo 1: gravando Hit
 * Modo 2: aguardando liberação
 */

//Queue
QueueHandle_t queue;
boolean ready = false;
boolean transmit = false;

//Credenciais para acesso à rede do Lactec
const char *networkName = "Lactec-Guest";
const char *networkPswd = "lactec123";

//Endereço e porta para comunicação UDP
const char *host = "192.168.0.191";
const int port = 3333;

//Está conectado?
boolean connected = false;

//Client WiFi
WiFiClient client;

char ssid1[15];
char ssid2[15];
String ssidESP = "";

//FreeRTOS
static int core1 = 0;
static int core2 = 1;

//Analog read
float analogValue = 0;
int meanSize = 10;
int pinID = 36;

void meanPinValue(void *pvParameters)
{
  unsigned long start;
  unsigned long current;
  unsigned long duration;

  String taskMessage = "Media sendo executada no core ";
  taskMessage = taskMessage + xPortGetCoreID();

  while (true)
  {
    float pinValue = 0.0;
    float weight = float(meanSize);
    for (int rd = 0; rd < meanSize; rd++)
    {
      pinValue = pinValue + analogRead(pinID) / weight;
    }

    switch (modo)
    {
    case 0: // Aguardando Hits
      xQueueSend(queue, &pinValue, portMAX_DELAY);
      if (pinValue > thr)
      {
        modo = 1;
        Serial.println("HIT!!");
        start = esp_timer_get_time();
        hitPointer = 1;
      }
      else
      {
        int sizeQ = uxQueueMessagesWaiting(queue);
        if (sizeQ > preTrigger)
        {
          // retirada de elementos da fila caso essa esteja maior que o preTrigger
          float aux;
          xQueueReceive(queue, &aux, 5);
        }
      }
      break;

    case 1: // Gravando Hit
      xQueueSend(queue, &pinValue, portMAX_DELAY);
      hitPointer++;
      if (hitPointer == afterTrigger)
      {
        current = esp_timer_get_time();
        duration = current - start;
        modo = 2;
        hitPointer = 0;
        Serial.println("Terminou de gravar o Hit em " + String(duration) + " us");
        transmit = true;
        ready = false;
      }
      break;

    case 2: // Aguardando liberação
      if (ready)
      {
        modo = 0;
      }
      vTaskDelay(1);
      break;
    }
  }
}
void setup()
{
  Serial.begin(115200);
  delay(1000);

  pinMode(LED, OUTPUT);

  //Queue
  queue = xQueueCreate(hitLenght, sizeof(float));
  if (queue == NULL)
  {
    Serial.println("Error creating the queue");
  }

  //Connect to the WiFi network
  connectToWiFi(networkName, networkPswd);

  //Obtem dados do chip
  uint64_t chipid = ESP.getEfuseMac(); // The chip ID is essentially its MAC address(length: 6 bytes).
  uint16_t chip = (uint16_t)(chipid >> 32);

  snprintf(ssid1, 15, "%04X", chip);
  snprintf(ssid2, 15, "%08X", (uint32_t)chipid);
  ssidESP = String(ssid1) + String(ssid2);

  //Definição dos pinos para leitura dos DrawWires
  pinMode(pinID, OUTPUT);

  Serial.print("Starting to create task on core ");
  Serial.println(core2);

  xTaskCreatePinnedToCore(
      meanPinValue,   /* Function to implement the task */
      "meanPinValue", /* Name of the task */
      10000,          /* Stack size in words */
      NULL,           /* Task input parameter */
      1,              /* Priority of the task */
      NULL,           /* Task handle. */
      core2);         /* Core where the task should run */

  Serial.println("Task created...");
}

void loop()
{

  Serial.println("Starting main loop...");
  ready = true;

  while (true)
  {

    float element;
    boolean received;

    if (transmit)
    {
      if (hitPointer == 0){
        client.print("NEW\r\n");
      }
      received = xQueueReceive(queue, &element, 5);
      if (not client.connected())
      {
        client.connect(host, port);
      }
      client.print(String(element) + "\r\n");
      hitPointer++;

      if ((hitPointer == hitLenght) | not received)
      {
        hitPointer = 0;
        client.print("END\r\n");
        Serial.println("Terminou de transmitir o Hit!!");
        transmit = false;
        ready = true;
      }
    }
    else
    {
      delay(1);
    }
  }
}

void connectToWiFi(const char *ssid, const char *pwd)
{
  Serial.println("Connecting to WiFi network: " + String(ssid));

  // delete old config
  WiFi.disconnect(true);
  //register event handler
  WiFi.onEvent(WiFiEvent);

  //Initiate connection
  WiFi.begin(ssid, pwd);

  Serial.println("Waiting for WIFI connection...");
}

//wifi event handler
void WiFiEvent(WiFiEvent_t event)
{
  switch (event)
  {
  case SYSTEM_EVENT_STA_GOT_IP:
    //When connected set
    Serial.print("WiFi connected! IP address: ");
    Serial.println(WiFi.localIP());
    //initializes the UDP state
    //This initializes the transfer buffer
    client.connect(host, port);
    connected = true;
    break;
  case SYSTEM_EVENT_STA_DISCONNECTED:
    Serial.println("WiFi lost connection");
    connected = false;
    break;
  }
}
